extern crate regex;
extern crate structopt;

mod config;
mod dirs;
mod files;
#[macro_use]
mod macros;
mod os_release;
mod result;
mod skel;
mod util;

use std::path::PathBuf;

use structopt::StructOpt;

use config::Config;
use result::Result;

fn main() -> Result<()> {
	let config: Config = StructOpt::from_args();

	if config.verbose {
		println!(
			"Building root fs for {:?} in {}",
			&config.files,
			&config.root_dir.display()
		);
	}

	dirs::check(&config)?;
	let mkrootfiles = files::Files::gather(&config)?;
	dirs::create(&config)?;
	mkrootfiles.copy(&config)?;

	let mut d = PathBuf::from(&config.root_dir);
	d.push("lib");
	files::set_linker_permissions(&mkrootfiles.libs, &d, config.verbose)?;

	let mut d = PathBuf::from(&config.root_dir);
	d.push("lib64");
	files::set_linker_permissions(&mkrootfiles.lib64s, &d, config.verbose)?;

	if let Err(e) = os_release::os_release(&config) {
		return serr!("Error creating etc/os-release: ", &e.to_string());
	}

	if config.skel != PathBuf::new() {
		skel::copy(&config)?;
	}

	Ok(())
}
