#[macro_export]
macro_rules! serr {
	($($arg:tt)*) => {
		Err(Box::from([$($arg)*].concat()))
	};
}
