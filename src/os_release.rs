use std::fs::{metadata, set_permissions, File};
use std::io::{BufWriter, Write};
use std::os::unix::fs::PermissionsExt;
use std::path::PathBuf;

use crate::config::Config;
use crate::result::Result;
use crate::serr;

pub fn os_release(config: &Config) -> Result<()> {
	let mut path = PathBuf::from(&config.root_dir);
	path.push("etc/os-release");

	if config.verbose {
		println!("Creating file {}", &path.display());
	}

	{
		let f = File::create(&path)?;
		let mut writer = BufWriter::new(f);
		writer.write_fmt(format_args!(
			"NAME=\"{}\"\nVERSION=\"{}\"\n",
			&config.osname, &config.osversion
		))?;
	}

	match metadata(&path) {
		Ok(meta) => {
			let mut perms = meta.permissions();
			perms.set_mode(0o644);
			if let Err(e) = set_permissions(&path, perms) {
				return serr!(
					"Error setting permissions ",
					&path.display().to_string(),
					": ",
					&e.to_string()
				);
			}
		}
		Err(e) => {
			return serr!(
				"Error reading metadata ",
				&path.display().to_string(),
				": ",
				&e.to_string()
			);
		}
	}

	Ok(())
}
