use std::path::PathBuf;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "mkroot")]
pub struct Config {
	/// OS name (for /etc/os-release)
	#[structopt(long, name = "OSNAME", default_value = "mkroot")]
	pub osname: String,

	/// OS version (for /etc/os-release)
	#[structopt(long, name = "OSVERSION", default_value = "1.0")]
	pub osversion: String,

	/// Optional skel directory to be copied after rootfs has been made
	#[structopt(long, name = "SKEL", default_value = "")]
	pub skel: PathBuf,

	/// Verbose
	#[structopt(short, long)]
	pub verbose: bool,

	/// Path to LDD
	#[structopt(long, name = "LDD", default_value = "/usr/bin/ldd")]
	pub ldd: PathBuf,

	/// Directory for root filesystem
	#[structopt(required = true, name = "DIR")]
	pub root_dir: PathBuf,

	/// Files to parse
	#[structopt(required = true, name = "FILES")]
	pub files: Vec<PathBuf>,
}
