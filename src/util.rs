use std::fs::{copy, create_dir, metadata, set_permissions, Permissions};
use std::path::PathBuf;

use crate::result::Result;
use crate::serr;

pub fn get_perms(path: &PathBuf) -> Result<Permissions> {
	match metadata(path) {
		Ok(meta) => Ok(meta.permissions()),
		Err(e) => serr!(
			"Error retrieving metadata ",
			&path.display().to_string(),
			": ",
			&e.to_string()
		),
	}
}

pub fn set_perms(path: &PathBuf, perms: Permissions) -> Result<()> {
	if let Err(e) = set_permissions(path, perms) {
		return serr!(
			"Error setting permissions ",
			&path.display().to_string(),
			": ",
			&e.to_string(),
		);
	}

	Ok(())
}

pub fn mkdir(dir: &PathBuf) -> Result<()> {
	if let Err(e) = create_dir(&dir) {
		return serr!(
			"Error creating directory ",
			&dir.display().to_string(),
			": ",
			&e.to_string()
		);
	}

	Ok(())
}

pub fn copy_file(src: &PathBuf, dst: &PathBuf) -> Result<()> {
	if let Err(e) = copy(&src, &dst) {
		return serr!(
			"Error copying file from ",
			&src.display().to_string(),
			" to ",
			&dst.display().to_string(),
			": ",
			&e.to_string()
		);
	}

	Ok(())
}
