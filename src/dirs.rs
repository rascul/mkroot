use std::fs::read_dir;
use std::path::PathBuf;

use crate::config::Config;
use crate::result::Result;
use crate::serr;
use crate::util::mkdir;

static DIRS: &[&str] = &[
	"bin", "dev", "etc", "home", "lib", "lib64", "proc", "root", "run", "sbin", "sys", "tmp",
	"usr", "var", "usr/bin",
];

pub fn check(config: &Config) -> Result<()> {
	if !&config.root_dir.exists() {
		return Ok(());
	}

	open(&config.root_dir, config.verbose)?;

	for dir in DIRS {
		let mut d = PathBuf::from(&config.root_dir);
		d.push(&dir);
		open(&d, config.verbose)?;
	}

	Ok(())
}

fn open(dir: &PathBuf, verbose: bool) -> Result<()> {
	if dir.exists() {
		if verbose {
			println!("Checking directory {}", &dir.display());
		}
		if let Err(e) = read_dir(&dir) {
			return serr!(
				"Error opening directory ",
				&dir.display().to_string(),
				": ",
				&e.to_string()
			);
		}
	}

	Ok(())
}

pub fn create(config: &Config) -> Result<()> {
	if config.verbose {
		println!("Creating directory {}", &config.root_dir.display());
	}

	mkdir(&config.root_dir)?;

	for dir in DIRS {
		let mut d = PathBuf::from(&config.root_dir);
		d.push(&dir);

		if config.verbose {
			println!("Creating directory {}", dir);
		}

		mkdir(&d)?;
	}

	Ok(())
}
