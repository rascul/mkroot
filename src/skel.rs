use std::fs::read_dir;
use std::os::unix::fs::PermissionsExt;
use std::path::PathBuf;

use crate::config::Config;
use crate::result::Result;
use crate::util::{copy_file, get_perms, mkdir, set_perms};

pub fn copy(config: &Config) -> Result<()> {
	if config.verbose {
		println!(
			"Copying skel {} to {}",
			&config.skel.display(),
			&config.root_dir.display()
		);
	}

	copy_dir(&config.skel, &config.root_dir, config.verbose)
}

fn copy_dir(src: &PathBuf, dst: &PathBuf, verbose: bool) -> Result<()> {
	match read_dir(&src) {
		Ok(src_dir) => {
			for entry in src_dir {
				match entry {
					Ok(entry_match) => {
						let entry_path = entry_match.path();
						let mut new_dst = PathBuf::from(&dst);
						new_dst.push(&entry_match.file_name());

						if entry_path.is_file() {
							if verbose {
								println!(
									"Copying {} to {}",
									&entry_path.display(),
									&new_dst.display()
								);

								copy_file(&entry_path, &new_dst)?;
							}
						} else if entry_path.is_dir() {
							if !new_dst.exists() {
								if verbose {
									println!("Creating directory {}", &new_dst.display());
								}
								mkdir(&new_dst)?;
							} else if !new_dst.is_dir() {
								return serr!(
									"Error: skel ",
									&entry_path.display().to_string(),
									" is a directory but target ",
									&new_dst.display().to_string(),
									" isn't"
								);
							}

							let entry_perms = get_perms(&entry_path)?;
							let dst_perms = get_perms(&new_dst)?;

							if entry_perms != dst_perms {
								if verbose {
									println!(
										"Setting permissions {:o} on {}",
										&entry_perms.mode(),
										&new_dst.display()
									);
								}

								set_perms(&new_dst, entry_perms)?;
							}

							copy_dir(&entry_path, &new_dst, verbose)?;
						}
					}
					Err(e) => {
						return serr!(
							"Error getting directory entry in ",
							&src.display().to_string(),
							": ",
							&e.to_string()
						);
					}
				}
			}
		}
		Err(e) => {
			return serr!(
				"Error opening directory ",
				&src.display().to_string(),
				": ",
				&e.to_string()
			);
		}
	};

	Ok(())
}
