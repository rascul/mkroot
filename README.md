build a minimal chroot/container/vm filesystem by taking a list of files and
putting them and their necessary libs into a target rootfs directory

this is linux only

example:

```
rascul@coyote:~/mkroot$ cargo run -- -h
    Finished dev [unoptimized + debuginfo] target(s) in 0.03s
     Running `target/debug/mkroot -h`
mkroot 0.1.0
rascul <rascul3@gmail.com>

USAGE:
    mkroot [FLAGS] [OPTIONS] <DIR> <FILES>...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Verbose

OPTIONS:
        --ldd <LDD>                Path to LDD [default: /usr/bin/ldd]
        --osname <OSNAME>          OS name (for /etc/os-release) [default: mkroot]
        --osversion <OSVERSION>    OS version (for /etc/os-release) [default: 1.0]

ARGS:
    <DIR>         Directory for root filesystem
    <FILES>...    Files to parse

rascul@coyote:~/mkroot$ cargo run -- -v rootfs /bin/bash /usr/bin/tree
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/mkroot -v rootfs /bin/bash /usr/bin/tree`
Building root fs for ["/bin/bash", "/usr/bin/tree"] in rootfs
Checking /bin/bash
Adding /lib64/libreadline.so.7
Adding /lib64/libdl.so.2
Adding /lib64/libc.so.6
Adding /lib64/libtinfo.so.6
Adding /lib64/ld-linux-x86-64.so.2
Checking /usr/bin/tree
Adding /lib64/libc.so.6
Adding /lib64/ld-linux-x86-64.so.2
Creating directory rootfs
Creating directory rootfs/bin
Creating directory rootfs/dev
Creating directory rootfs/etc
Creating directory rootfs/home
Creating directory rootfs/lib
Creating directory rootfs/lib64
Creating directory rootfs/proc
Creating directory rootfs/root
Creating directory rootfs/run
Creating directory rootfs/sbin
Creating directory rootfs/sys
Creating directory rootfs/tmp
Creating directory rootfs/usr
Creating directory rootfs/var
Creating directory rootfs/usr/bin
Copying /bin/bash to rootfs/bin/bash
Copying /usr/bin/tree to rootfs/bin/tree
Copying /lib64/ld-linux-x86-64.so.2 to rootfs/lib64/ld-linux-x86-64.so.2
Copying /lib64/libc.so.6 to rootfs/lib64/libc.so.6
Copying /lib64/libdl.so.2 to rootfs/lib64/libdl.so.2
Copying /lib64/libreadline.so.7 to rootfs/lib64/libreadline.so.7
Copying /lib64/libtinfo.so.6 to rootfs/lib64/libtinfo.so.6
Setting linker rootfs/lib64/ld-linux-x86-64.so.2 to mode 0o755
Creating file etc/os-release

rascul@coyote:~/mkroot$ sudo chroot rootfs /bin/bash

bash-4.4# exit
exit

rascul@coyote:~/mkroot$ wget -q -O rootfs/sbin/init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64

rascul@coyote:~/mkroot$ chmod 0755 rootfs/sbin/init

rascul@coyote:~/mkroot$ sudo systemd-nspawn -D rootfs/ /sbin/init /bin/bash
Spawning container rootfs on /home/rascul/mkroot/rootfs.
Press ^] three times within 1s to kill container.
Timezone America/Chicago does not exist in container, not updating container timezone.

bash-4.4# tree -x
.
|-- bin
|   |-- bash
|   `-- tree
|-- dev
|-- etc
|   |-- os-release
|   `-- resolv.conf
|-- home
|-- lib
|-- lib64
|   |-- ld-linux-x86-64.so.2
|   |-- libc.so.6
|   |-- libdl.so.2
|   |-- libreadline.so.7
|   `-- libtinfo.so.6
|-- proc
|-- root
|-- run
|-- sbin
|   `-- init
|-- sys
|-- tmp
|-- usr
|   `-- bin
`-- var
    `-- log
        `-- journal

17 directories, 10 files

bash-4.4# exit
exit
Container rootfs exited successfully.
```
